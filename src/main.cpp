#include <iostream>
#include <vector>
#include <string>
#include <memory>
#include <map>
#include <fstream>
#include <algorithm>
#include <set>
#include <iterator>
#include <unordered_set>

#include "Bridger.h"

using namespace std;

int main(int argc, char **argv)
{
  string strReadsFasta = argv[1];
  string strMMPaf = argv[2];
  string strContigsFasta = argv[3];

  ezra::Bridger bridger(strReadsFasta, strMMPaf, strContigsFasta);
  bridger.Execute();

  return 0;
}
