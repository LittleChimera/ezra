#include "Bridger.h"
#include "Extension.h"
#include "Parser.h"
#include "Graph.h"
#include <vector>
#include <string>
#include <iostream>

namespace ezra {

using namespace std;

Bridger::Bridger(const string& strReads, const string& strPaf, const string& strContigs) {
  Initialize(strReads, strPaf, strContigs);
}

void Bridger::Initialize(const string& strReads, const string& strPaf, const string& strContigs) {
    parseProcessFastq(strReads, mIdToRead);
    parseProcessFasta(strContigs, mIdToContig);
    parseProcessPafExt(strPaf, mIdToExt);
}

void Bridger::Execute(void) {
  vector<string> aContigIds;
  for(auto& it : mIdToContig) {
    aContigIds.emplace_back(it.first);
  }
  vector<ezra::Edge> aEdges = ezra::CreateChains(aContigIds, mIdToExt);
  for (const auto& edge : aEdges) {
    cerr << "Bridge: " << edge.strX << " " << edge.eX << " "<< edge.strY << " " << edge.eY << " "<< edge.ulCoverage << endl;
  }

  ConsumeEdges(aEdges, mIdToContig, mIdToRead, mIdToExt);
}

}
