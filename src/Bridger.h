#include "Types.h"

namespace ezra {

class Bridger {
public:
  MapIdToExt mIdToExt;
  MapIdToSeq mIdToContig;
  MapIdToSeq mIdToRead;

  Bridger(const std::string& strReads, const std::string& strPaf, const std::string& strContigs);

  void Initialize(const std::string& strReads, const std::string& strPaf, const std::string& strContigs);

  void Execute(void);
};

}
