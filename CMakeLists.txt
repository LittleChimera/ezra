cmake_minimum_required(VERSION 3.5)
project(ezra)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic -march=native")
set(CMAKE_CXX_STANDARD 14)

add_subdirectory(vendor/bioparser EXCLUDE_FROM_ALL)
add_subdirectory(vendor/spoa EXCLUDE_FROM_ALL)

set(SOURCE_FILES src/main.cpp src/Sequence.cpp src/Extension.cpp src/Graph.cpp src/Bridger.cpp src/Parser.cpp)

add_executable(ezra ${SOURCE_FILES})

target_link_libraries(ezra bioparser spoa)
